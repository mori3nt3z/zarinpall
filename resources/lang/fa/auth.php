<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'نام کاربری یا رمز عبور اشتباه است',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'welcome' => 'خوش آمدید',
    'enterYourEmail' => 'پست الکترونیکی ',
    'enterYourPassword' => ' رمز عبور',
    'logIn' => 'ورود',
    'signInWith' => 'ورود با شبکه های اجتماعی',
    'RememberMe' => 'من را به خاطر بسپار',
    'forgotYourPassword' => 'فراموشی رمز عبور',
    'signUp' => 'ثبت نام',
    'Donthaveanaccount' => 'هنوز ثبت نام نکردید ؟',
    'name' => 'نام کاربری',
    'email' => 'پست الکترونیکی',
    'password' => 'رمز عبور ',
    'passwordConfirmation' => 'تکرار رمز عبور',
    'iAccept' => 'مطالعه کردم',
    'termsAndConditions' => 'قوانین و شرایط را',
    'phone_number' => 'شماره همراه',
    'recaptcha' => 'تیک من ربات نیستم را بزنید'
];
