@extends('layouts.auth')
@section('content')
<div class="card">

    <div class="card-body p-4">
        
        <div class="text-center w-75 m-auto">
            <div class="auth-logo">
                <a href="index.html" class="logo logo-dark text-center">
                    <span class="logo-lg">
                        <img src="../assets/images/logo-dark.png" alt="" height="22">
                    </span>
                </a>

                <a href="index.html" class="logo logo-light text-center">
                    <span class="logo-lg">
                        <img src="../assets/images/logo-light.png" alt="" height="22">
                    </span>
                </a>
            </div>
        </div>
        @include('auth.partials.validation')

        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="mb-2">
                <label for="name" class="form-label">@lang('auth.name')</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required >
            </div>
            <div class="mb-2">
                <label for="email" class="form-label">@lang('auth.email')</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required >
            </div>
            <div class="mb-2">
                <label for="phone_number" class="form-label">@lang('auth.phone_number')</label>
                <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}" required >
            </div>
            <div class="mb-2">
                <label for="password" class="form-label">@lang('auth.password')</label>
                <div class="input-group input-group-merge">
                    <input id="password" type="password" class="form-control" name="password" required >
                </div>
            </div>
            <div class="mb-2">
                <label for="password_confirmation" class="form-label">@lang('auth.passwordConfirmation')</label>
                <div class="input-group input-group-merge">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required >
                </div>
            </div>

            <div class="mb-2">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="checkbox-signup" name="terms" >
                    <label class="form-check-label  ml-3" for="checkbox-signup">
                         <a href="javascript: void(0);" class="text-dark">@lang('auth.termsAndConditions')</a> @lang('auth.iAccept')
                    </label>
                </div>
                  
            </div>
            <div class="mb-3">
               @include('auth.partials.recaptcha')
            </div>

            <div class="d-grid text-center">
                <button class="btn btn-primary" type="submit" value="@lang('auth.signUp')">@lang('auth.signUp')</button>
            </div>

        </form>


    </div> <!-- end card-body -->
</div>
@endsection
 

