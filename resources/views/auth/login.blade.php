@extends('.layouts.auth')
@section('content')


    <div class="card">

        <div class="card-body p-4">

            <div class="text-center w-75 m-auto">
                <a href="index.html">
                    <span><img src="assets/images/logo-dark.png" alt="" height="22"></span>
                </a>
                <p class="text-muted mb-4 mt-3">@lang('auth.welcome')</p>
            </div>

            @include('auth.partials.validation')
            <form method="post" action="{{ route('login') }}">
                @csrf

                <div class="form-group mb-3">
                    <label for="emailaddress">@lang('auth.enterYourEmail')</label>
                    <input class="form-control" name="email" type="email" value="{{ old('email')}}">
                </div>

                <div class="form-group mb-3">
                    <label for="password">@lang('auth.enterYourPassword')</label>
                    <input class="form-control" name="password" type="password" >
                </div>

                <div class="form-group mb-3">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="rememberMe" class="custom-control-input" id="checkbox-signin"
                            checked="">
                        <label class="custom-control-label" for="checkbox-signin">@lang('auth.RememberMe')</label>
                    </div>
                </div>
                <div class="form-group mb-3">
                    @include('auth.partials.recaptcha')
                </div>

                <div class="form-group mb-0 text-center">
                    <input class="btn btn-primary btn-block" type="submit" value="@lang('auth.logIn')"></input>
                </div>

            </form>

            <div class="text-center">
                <h5 class="mt-3 text-muted">@lang('auth.signInWith')</h5>
                <ul class="social-list list-inline mt-3 mb-0">
                    <li class="list-inline-item">
                        <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i
                                class="mdi mdi-facebook"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i
                                class="mdi mdi-google"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="javascript: void(0);" class="social-list-item border-info text-info"><i
                                class="mdi mdi-twitter"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="javascript: void(0);" class="social-list-item border-secondary text-secondary"><i
                                class="mdi mdi-github-circle"></i></a>
                    </li>
                </ul>
            </div>

        </div> <!-- end card-body -->
    </div>
    <!-- end card -->

    <div class="row mt-3">
        <div class="col-12 text-center">
            <p> <a href="pages-recoverpw.html" class="text-white ml-1">@lang('auth.forgotYourPassword')</a></p>
            <p class="text-white">@lang('auth.Donthaveanaccount')<a href="{{ route('register')}}"
                    class="text-primary font-weight-medium ml-1">@lang('auth.signUp')</a></p>
        </div> <!-- end col -->
    </div>
    <!-- end row -->




@endsection
