<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <title>Minton - Responsive Admin Dashboard Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/assets/images/favicon.ico') }}">

    <!-- recaptcha link -->
    <script src="https://www.google.com/recaptcha/api.js?hl=fa" async defer></script>

   

    <!-- App css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/app-rtl.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />


    @yield('css')
</head>


<body class="mori">
<style>
    .mori{
        background: #005aa7; /* fallback for old browsers */
  background: -webkit-linear-gradient(to right, #005aa7, #fffde4); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(to right, #005aa7, #fffde4); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }
</style>
    <!-- Begin page -->
    <div id="wrapper">
        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-4">


                        <!-- ============================================================== -->
                        <!-- Start Page Content here -->
                        <!-- ============================================================== -->

                        @yield('content')

                        <!-- ============================================================== -->
                        <!-- End Page content -->
                        <!-- ============================================================== -->

                    </div>
                </div>
            </div>
        </div>

    </div>



    <!-- Vendor js -->
    <script src="{{ asset('assets/js/vendor.min.js') }} "></script>

    <script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }} "></script>
    <script src="{{ asset('assets/libs/jquery-sparkline/jquery.sparkline.min.js') }} "></script>

    <!-- Peity chart-->
    <script src="{{ asset('assets/libs/peity/jquery.peity.min.js') }} "></script>

    <!-- init js -->
    <script src="{{ asset('assets/js/pages/dashboard-2.init.js') }} "></script>

    <!-- App js -->
    <script src="{{ asset('assets/js/app.min.js') }} "></script>
    @yield('scripts')
</body>

</html>
